# PyRates

Format fake rates from FakeEfficiencyTool for use with FakeBkgTools

## usage
Make sure the parent directory is in your PYTHONPATH, then
```
usage: PyRates [-h] [--prefix PREFIX] [-d DIR] [-p PAINT] [-P] [-b] [-l LUMI]
               [-q] [-v]
               configFile fakeEfficiencies realEfficiencies
               [promptEfficiencies [promptEfficiencies ...]]
```
where fakeEfficiencies, realEfficiencies and promptEfficiencies each specify
a .txt file with a list of root files which will be hadd-summed over.

## config
Config file format is similar to AnalysisTop config files. You define
SELECTION-sections that are named and contain the following keys:
```
fake: TDirectory within root files that contains fake rates
real: TDirectory within root files that contains real rates
namePass: first part of name of histogram containing passing events
nameTot: first part of name of histogram containing total events
lumi: target luminosity for scaling prompt rates
param: parametrization name, second part of histogram names
outfile: specify an outfile in order to write several selections to a single outfile
lepton: specify a lepton name to produce histograms with FakeBkgTools-compatible naming-convention (FakeEfficiency2D_{lepton}_{param})
```
the tool will load the following histograms:
```
fake rates pass: <fakeEfficiencies>/<fake>/<namePass>_<param>
fake rates total: <fakeEfficiencies>/<fake>/<nameTot>_<param>

real rates pass: <realEfficiencies>/<real>/<namePass>_<param>
real rates total: <realEfficiencies>/<real>/<nameTot>_<param>

prompt rates pass: <promptEfficiencies>/<fake>/<namePass>_<param>
prompt rates totel: <promptEfficiencies>/<fake>/<nameTot>_<param>
```
for each of the root files in fake-, real-, promptEfficiencies.

To reduce boilerplate SELECTIONs can be composed of SUB-selections.

To get an idea see [ExampleConfig.txt](misc/ExampleConfig.txt)
For advanced configuration options see [config_advanced.txt](test/config_advanced.txt)

A default configuration to use with vanilla FakeEfficiencyTool would be:
```
SUB .el
namePass histo2D_Tight_el
nameTot histo2D_Loose_el
lepton el

SUB .mu
namePass histo2D_Tight_mu
nameTot histo2D_Loose_mu
lepton mu

SUB .base
fake Efficiencies_{selection}
real Efficiencies_{selection}
prompt Efficiencies_{selection}
lumi 3813
param pt_eta
outfile Efficiency_2D.root

FOR selection IN Selection_1 Selection_2 Selection_3 Selection_4

SELECTION {selection}_el
.base
.el

SELECTION {selection}_mu
.base
.mu

ENDFOR
```

## how it works
For each SELECTION from the config file, a 2D histogram named 
`FakeEfficiency2D_<lepton>_<param>` and one named 
`RealEfficiency2D_<lepton>_<param>` will be written to a file that is either
named after the selection name or for which a name can be specified in the
`outfile`-config-parameter.

The outfiles will be written to a directory that can be specified via the `-d`
CLI-option. The default is "Efficiency_2D".

This Fake-histogram contains:
```
(fake rates pass - prompt rates pass) / (fake rates total - prompt rates total)
```

The Real-histogram contains:
```
real rates pass / real rates total
```

prompt rates pass and prompt rates total means histograms will be hadd-ed for
each of the promptEfficiencies-parameters passed to the cli, then scaled to
match target lumi, then hadd-ed over all the promptEfficiencies-parameters.

## zsh-completion
for zsh-autocompletion
* copy `/misc/completion/_PyRates` to `~/.zsh/completion/_PyRates`
* add the following lines to your `.zshrc`:
```
# use PyRates instead of python -m PyRates
function PyRates() {
    python -m PyRates $@
}
# custom completions
fpath=(~/.zsh/completion $fpath)
```

then use like `PyRates config.txt fake.txt real.txt prompt1.txt prompt2.txt ...`


## tests
currently contain hard-coded paths on my system, sorry.
