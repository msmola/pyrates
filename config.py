class Selection:
    def __init__(self, name, forVars = None, config = None, **kwargs):
        self.name = name.strip()
        self.forVars = list(forVars) if forVars else None
        self.config = config or kwargs
    
    def set(self, key, value):
        self.config[key] = value.strip()

    def sub(self, sub):
        for key, value in sub.config.items():
            self.config[key] = value

    def get(self, key):
        return self.config[key]

    def hasKeys(self, *args):
        for key in args:
            if key not in self.config:
                raise KeyError(
                        "Selection {name} is missing config parameter {key}".format(
                            name=self.name,
                            key=key
                            )
                        )
    def default(self, **kwargs):
        for key, value in kwargs.items():
            if key not in self.config:
                self.config[key] = value

    def each(self):
        if not self.forVars:
            return [self]
        repls = [{}]
        for var, values in self.forVars:
            newRepls = []
            for value in values:
                for repl in repls:
                    newRepl = dict(repl)
                    newRepl[var] = value
                    newRepls.append(newRepl)
            repls = newRepls
        allSelections = [self.repl(repl) for repl in repls]
        return allSelections
        
    def repl(self, repl):
        name = self.name.format(**repl)
        config = {}
        for key, old in self.config.items():
            config[key] = old.format(**repl)
        return Selection(name, config=config)

class Binning:
    def __init__(self, name, x = None, y = None):
        self.name = name.strip()
        self.x = x
        self.y = y

    def set(self, key, value):
        if key == 'x':
            self.x = [float(edge) for edge in value.split(',')]
        elif key == 'y':
            self.y = [float(edge) for edge in value.split(',')]
        else:
            raise ValueError('Trying to set unknown key "{}" for Binning.'.format(key))

    def getXedges(self):
        return self.x
    def getYedges(self):
        return self.y

class InputFiles:
    def __init__(self):
        self.fake = None
        self.real = None
        self.prompt = []

    def set(self, key, value):
        if key == 'fake':
            self.fake = value
        elif key == 'real':
            self.real = value
        elif key == 'prompt':
            self.prompt.append(value)
        else:
            raise KeyError("Unknown key {} in InputFile configuration".format(key))

    def getFake(self):
        return self.fake
    def getReal(self):
        return self.real
    def getPrompts(self):
        return self.prompt

class Config:
    def __init__(self, filename = None):
        self.subs = {}
        self.selections = {}
        self.forVars = []
        self.binnings = {}
        self.inputFiles = InputFiles()
        if not filename:
            return
        with open(filename) as cf:
            lines = cf.readlines()
        current = None
        while len(lines):
            line = lines.pop(0).split('#', 1)[0]
            if line.strip() == '':
                continue
            if line.startswith('.'):
                current.sub(self.subs[line.strip()])
                continue
            if line.startswith('ENDFOR'):
                self.forVars.pop()
                continue
            (key, value) = line.split(' ', 1)
            if key == 'SUB':
                current = Selection(value, forVars = self.forVars)
                self.subs[current.name] = current
                continue
            if key == 'SELECTION':
                current = Selection(value, forVars = self.forVars)
                self.selections[current.name] = current
                continue
            if key == 'BINNING':
                current = Binning(value)
                self.binnings[current.name] = current
                continue
            if key == 'FOR':
                var, values = value.split(' IN ', 1)
                values = values.strip().split(' ')
                self.forVars.append((var, values))
                continue
            if key == 'INPUT':
                current = self.inputFiles
                continue
            if key == 'INCLUDE':
                with open(value.strip()) as icf:
                    lines = icf.readlines() + lines
                continue
                    
            current.set(key, value)
                    
    def add(self, name, selection):
        self.selections[name] = selection

    def getSelections(self):
        return sum([s.each() for s in self.selections.values()], [])

    def getBinnings(self):
        return self.binnings

    def getFakeInputFile(self):
        return self.inputFiles.getFake()
    def getRealInputFile(self):
        return self.inputFiles.getReal()
    def getPromptInputFiles(self):
        return self.inputFiles.getPrompts()
