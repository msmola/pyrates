import argparse
from plotRates import plotRates2D, defaultDir
from config import Config

description = ('Plot real and fake efficiencies from FakeEfficiencyTool'
        'to a root file for further processing using FakeBkgTools')
parser = argparse.ArgumentParser(description = description, prog='PyRates')

parser.add_argument('configFile', type=str, help=(
    'Config file, format: \'key value\', one pair per line.'
    ))
parser.add_argument('fakeEfficiencies', type=str)
parser.add_argument('realEfficiencies', type=str)
parser.add_argument('promptEfficiencies', nargs='*', type=str, help=(
    'Add one or multiple lists of prompt files.\n'
    'Each list will be scaled to full Luminosity.'
    ))
parser.add_argument('--prefix', type=str, default='')
parser.add_argument('-d', '--dir', type=str, default=defaultDir)
parser.add_argument('-p', '--paint', type=str, default='pdf')
parser.add_argument('-P', '--nopaint', action='store_true')
parser.add_argument('-b', '--batch', action='store_true')
parser.add_argument('-l', '--lumi', type=float, default=None)
parser.add_argument('-q', '--quiet', action='store_true')
parser.add_argument('-v', '--verbose', action='store_true')

args = parser.parse_args()

config = Config(args.configFile)

plotRates2D(
        selections=config.getSelections(),
        binnings=config.getBinnings(),
        fakeEff=args.fakeEfficiencies or config.getFakeInputFile(),
        realEff=args.realEfficiencies or config.getRealInputFile(),
        promptEffs=args.promptEfficiencies or config.getPromptInputFiles(),
        dirName=args.dir,
        prefix=args.prefix,
        paint=None if args.nopaint else args.paint,
        lumi=args.lumi,
        quiet=args.quiet,
        verbose=args.verbose,
        )
