import math
import sys

def red(text):
    return '\033[91m{}\033[0m'.format(text)

class ProgressBar:
    position = 0
    total = None
    length = 0

    def __init__(self, total):
        self.position = 0
        self.total = total

    def advance(self):
        self.position += 1
        self.clear()
        self.paint()

    def finish(self, message = ''):
        self.position = self.total
        self.clear()
        self.paint()
        self.notify(message, False)

    def clear(self):
        sys.stdout.write('\b' * self.length)
        sys.stdout.flush()
        self.length = 0

    def paint(self):
        output = '[{:3d}%]'.format(
            self.position * 100 / self.total if self.total > 0 else 100
            )
        self.length = len(output)
        sys.stdout.write(output)
        sys.stdout.flush()

    def notify(self, message, paint = True):
        if self.length == 0:
            self.paint()
        sys.stdout.write(' {}\n'.format(message))
        if paint:
            self.paint()

    def warn(self, message, paint = True):
        if self.length != 0:
            sys.stdout.write('\n')
            sys.stdout.flush()
        sys.stderr.write(' {}\n'.format(red(message)))
        sys.stderr.flush()
        if paint:
            self.paint()

class ProgressCounter:
    position = 0
    total = None
    pct = 0

    def __init__(self, total):
        self.position = 0
        self.total = total
        self.pct = 0

    def advance(self):
        self.position += 1
        pct = int(self.position * 100 / self.total)
        if pct != self.pct:
            self.pct = pct
            self.clear()
            self.paint()

    def finish(self, message = ''):
        self.position = self.total
        self.pct = 100
        self.clear()
        self.paint()
        if message:
            sys.stdout.write(' {}\n'.format(message))

    def clear(self):
        sys.stdout.write('\b' * 6)
        sys.stdout.flush()

    def paint(self):
        sys.stdout.write('[{:3d}%]'.format(self.pct))
        sys.stdout.flush()

    def notify(self, message, paint = True):
        pass

    def warn(self, message, paint = True):
        sys.stdout.write('\n')
        sys.stdout.flush()
        sys.stderr.write(' {}\n'.format(red(message)))
        sys.stderr.flush()
        if paint:
            self.paint()
