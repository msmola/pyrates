SUB .el
namePass histo2D_Tight_el
nameTot histo2D_Loose_el
fake Efficiencies_Fake1L_Fake1e_{region}_2015,Efficiencies_Fake1L_Fake1e_{region}_2016
real Efficiencies_Real_Real2LOS_{region}_2015,Efficiencies_Real_Real2LOS_{region}_2016
prompt Efficiencies_Fake1L_Fake1e_{region}_2015

SUB .mu
namePass histo2D_Tight_mu
nameTot histo2D_Loose_mu
fake Efficiencies_Fake1L_Fake1mu_{region}_2015,Efficiencies_Fake1L_Fake1mu_{region}_2016
real Efficiencies_Real_Real2LOS_{region}_2015,Efficiencies_Real_Real2LOS_{region}_2016
prompt Efficiencies_Fake1L_Fake1mu_{region}_2015

#SUB .base
#lumi 3219.56 # 2015
#lumi 32988.1 # 2016
#lumi 44307.4 # 2017
#lumi 36207.66 # 2015 + 2016

FOR param IN pt_eta
FOR region IN ge2j

SELECTION el_{region}_{param}
#.base
.el
param {param}
lepton el
outfile {region}_{param}.root

SELECTION mu_{region}_{param}
#.base
.mu
param {param}
lepton mu
outfile {region}_{param}.root

#SELECTION el_MET_{region}_{param}
#.base
#.el
#fake Efficiencies_Fake1L_Fake1e_{region}_2015
#real Efficiencies_Real_Real2LOS_{region}_2015
#param {param}
#lepton el
#outfile MET_{region}_{param}.root
#
#SELECTION mu_MET_{region}_{param}
#.base
#.mu
#fake Efficiencies_Fake1L_Fake1mu_MET_{region}_2015
#real Efficiencies_Real_Real2LOS_MET_{region}_2015
#param {param}
#lepton mu
#outfile MET_{region}_{param}.root
#
#FOR mwet IN MWT MET_MWT
#
#SELECTION el_{mwet}_{region}_{param}
#.base
#.el
#fake Efficiencies_Fake1L_Fake1e_{mwet}_{region}_2015
#real Efficiencies_Real_Real2LOS_{mwet}_{region}_2015
#param {param}
#lepton el
#outfile {mwet}_{region}_{param}.root
#
#SELECTION mu_{mwet}_{region}_{param}
#.base
#.mu
#fake Efficiencies_Fake1L_Fake1mu_{mwet}_{region}_2015
#real Efficiencies_Real_Real2LOS_{mwet}_{region}_2015
#param {param}
#lepton mu
#outfile {mwet}_{region}_{param}.root
#
#ENDFOR

ENDFOR
ENDFOR
