from PyRates.plotRates import plotRates2D
from PyRates.config import Config
from os.path import dirname, join, isfile
import shutil
import ROOT

def test_advanced():
    config = Config(join(dirname(__file__), 'config_advanced.txt'))
    assert isinstance(config, Config)

    fldir = '/nfs/dust/atlas/user/smolamic/Data/filelists'

    plotRates2D(
            selections=config.getSelections(),
            fakeEff='{}/user.msmola.data15.Fake.25Feb19.txt'.format(fldir),
            realEff='{}/user.msmola.data15.Real.25Feb19.txt'.format(fldir),
            promptEffs=[
                '{}/user.msmola.mc16a.WJets.Prompt.25Feb19.364156.txt'.format(fldir),
                '{}/user.msmola.mc16a.singletop.Prompt.25Feb19.410658.txt'.format(fldir),
                ],
            #quiet=True
            verbose=True,
            )

    assert isfile('Efficiency_2D/ge2j_pt_eta.root')
    assert isfile('Efficiency_2D/ge4j_pt_eta.root')

    f = ROOT.TFile('Efficiency_2D/ge2j_pt_eta.root')
    h = f.Get('RealEfficiency2D_el_pt_eta')
    assert h.GetEntries() != 0

    f.Close()


    shutil.rmtree('Efficiency_2D')

    print 'test_advanced done.'

if __name__ == '__main__':
    test_advanced()
