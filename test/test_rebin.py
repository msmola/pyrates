from PyRates.plotRates import plotRates2D
from PyRates.config import Config
from os.path import dirname, join, isfile
import shutil
import ROOT

def test_rebin():
    config = Config(join(dirname(__file__), 'config_rebin.txt'))
    assert isinstance(config, Config)

    plotRates2D(
            selections=config.getSelections(),
            binnings=config.getBinnings(),
            fakeEff=join(dirname(__file__), 'data15.Fake.19Aug19.txt'),
            realEff=join(dirname(__file__), 'data15.Real.19Aug19.txt'),
            verbose=True,
            )

    assert isfile('Efficiency_2D/el_ge2j1b_pt_eta.root')
    tf = ROOT.TFile('Efficiency_2D/el_ge2j1b_pt_eta.root')
    assert tf.IsOpen()
    fake = tf.Get('FakeEfficiency2D_el_pt_eta')
    assert isinstance(fake, ROOT.TH2)
    real = tf.Get('RealEfficiency2D_el_pt_eta')
    assert isinstance(real, ROOT.TH2)
    fake_pt = tf.Get('FakeEfficiency_el_pt')
    assert isinstance(fake_pt, ROOT.TH1)
    real_pt = tf.Get('RealEfficiency_el_pt')
    assert isinstance(real_pt, ROOT.TH1)
    fake_eta = tf.Get('FakeEfficiency_el_eta')
    assert isinstance(fake_eta, ROOT.TH1)
    real_eta = tf.Get('RealEfficiency_el_eta')
    assert isinstance(real_eta, ROOT.TH1)
    assert fake.GetNbinsX() == 5
    assert fake.GetNbinsY() == 4
    assert real.GetNbinsX() == 5
    assert real.GetNbinsY() == 4
    assert fake_pt.GetNbinsX() == 34
    assert fake_eta.GetNbinsX() == 24

    raw = ROOT.TFile(join(dirname(__file__), 'data15.Fake.19Aug19.root'))
    loose = raw.Get('Efficiencies_Fake1L_Fake1e_mwt20_metmwt60_ge2j1b_2015/histo2D_Loose_el_pt_eta')
    tight = raw.Get('Efficiencies_Fake1L_Fake1e_mwt20_metmwt60_ge2j1b_2015/histo2D_Tight_el_pt_eta')


    tight_pt = tight.ProjectionX()
    print tight_pt.GetName()
    tight_eta = tight.ProjectionY()
    print tight_eta.GetName()
    loose_pt = loose.ProjectionX()
    print loose_pt.GetName()
    loose_eta = loose.ProjectionY()
    print loose_eta.GetName()

    tight_pt.Divide(loose_pt)
    tight_eta.Divide(loose_eta)

    assert tight_pt.GetNbinsX() == fake_pt.GetNbinsX()
    for xbin in range(tight_pt.GetNbinsX() + 1):
        assert tight_pt.GetBinContent(xbin) == fake_pt.GetBinContent(xbin)

    assert tight_eta.GetNbinsX() == fake_eta.GetNbinsX()
    for xbin in range(tight_eta.GetNbinsX() + 1):
        assert tight_eta.GetBinContent(xbin) == fake_eta.GetBinContent(xbin)

    tf.Close()
    
    shutil.rmtree('Efficiency_2D')

    print 'test_rebin done.'


if __name__ == '__main__':
    test_rebin()
