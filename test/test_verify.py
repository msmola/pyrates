from PyRates.plotRates import plotRates2D
from PyRates.config import Config
from os.path import dirname, join, isfile
import shutil
import ROOT

def heq(h1, h2):
    for xbin in range(0, h1.GetNbinsX() + 1):
        for ybin in range(0, h1.GetNbinsY() + 1):
            assert h1.GetBinContent(xbin, ybin) == h2.GetBinContent(xbin, ybin)

def test_verify():
    config = Config(join(dirname(__file__), 'config_verify.txt'))
    assert isinstance(config, Config)

    fldir = '/nfs/dust/atlas/user/smolamic/Data/filelists'

    fakeEff=fldir + '/user.msmola.data15.Fake.25Feb19.txt'
    realEff=fldir + '/user.msmola.data15.Real.25Feb19.txt'

    promptEffs=[
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.WJets.Prompt.25Feb19.364170.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.WJets.Prompt.25Feb19.364171.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.WJets.Prompt.25Feb19.364172.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.WJets.Prompt.25Feb19.364173.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.WJets.Prompt.25Feb19.364174.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.WJets.Prompt.25Feb19.364175.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.WJets.Prompt.25Feb19.364176.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.WJets.Prompt.25Feb19.364177.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.WJets.Prompt.25Feb19.364178.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.WJets.Prompt.25Feb19.364179.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.WJets.Prompt.25Feb19.364180.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.WJets.Prompt.25Feb19.364181.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.WJets.Prompt.25Feb19.364182.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.WJets.Prompt.25Feb19.364183.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.WJets.Prompt.25Feb19.364156.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.WJets.Prompt.25Feb19.364157.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.WJets.Prompt.25Feb19.364158.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.WJets.Prompt.25Feb19.364159.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.WJets.Prompt.25Feb19.364160.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.WJets.Prompt.25Feb19.364161.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.WJets.Prompt.25Feb19.364162.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.WJets.Prompt.25Feb19.364163.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.WJets.Prompt.25Feb19.364164.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.WJets.Prompt.25Feb19.364165.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.WJets.Prompt.25Feb19.364166.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.WJets.Prompt.25Feb19.364167.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.WJets.Prompt.25Feb19.364184.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.WJets.Prompt.25Feb19.364185.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.WJets.Prompt.25Feb19.364186.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.WJets.Prompt.25Feb19.364187.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.WJets.Prompt.25Feb19.364189.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.WJets.Prompt.25Feb19.364190.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.WJets.Prompt.25Feb19.364192.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.WJets.Prompt.25Feb19.364193.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.WJets.Prompt.25Feb19.364194.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.WJets.Prompt.25Feb19.364195.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.WJets.Prompt.25Feb19.364196.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.WJets.Prompt.25Feb19.364197.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.ZJets.Prompt.05Apr19.364100.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.ZJets.Prompt.05Apr19.364101.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.ZJets.Prompt.05Apr19.364102.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.ZJets.Prompt.05Apr19.364103.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.ZJets.Prompt.05Apr19.364104.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.ZJets.Prompt.05Apr19.364105.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.ZJets.Prompt.05Apr19.364106.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.ZJets.Prompt.05Apr19.364107.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.ZJets.Prompt.05Apr19.364108.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.ZJets.Prompt.05Apr19.364109.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.ZJets.Prompt.05Apr19.364110.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.ZJets.Prompt.05Apr19.364111.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.ZJets.Prompt.05Apr19.364112.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.ZJets.Prompt.05Apr19.364113.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.ZJets.Prompt.05Apr19.364114.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.ZJets.Prompt.05Apr19.364115.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.ZJets.Prompt.05Apr19.364116.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.ZJets.Prompt.05Apr19.364117.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.ZJets.Prompt.05Apr19.364118.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.ZJets.Prompt.05Apr19.364119.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.ZJets.Prompt.05Apr19.364120.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.ZJets.Prompt.05Apr19.364121.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.ZJets.Prompt.05Apr19.364122.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.ZJets.Prompt.05Apr19.364123.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.ZJets.Prompt.05Apr19.364124.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.ZJets.Prompt.05Apr19.364125.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.ZJets.Prompt.05Apr19.364126.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.ZJets.Prompt.05Apr19.364127.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.ZJets.Prompt.05Apr19.364128.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.ZJets.Prompt.05Apr19.364129.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.ZJets.Prompt.05Apr19.364130.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.ZJets.Prompt.05Apr19.364131.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.ZJets.Prompt.05Apr19.364132.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.ZJets.Prompt.05Apr19.364133.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.ZJets.Prompt.05Apr19.364134.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.ZJets.Prompt.05Apr19.364135.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.ZJets.Prompt.05Apr19.364136.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.ZJets.Prompt.05Apr19.364137.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.ZJets.Prompt.05Apr19.364138.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.ZJets.Prompt.05Apr19.364139.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.ZJets.Prompt.05Apr19.364140.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.ZJets.Prompt.05Apr19.364141.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.diboson.Prompt.07Apr19.364250.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.diboson.Prompt.07Apr19.364253.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.diboson.Prompt.07Apr19.364254.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.diboson.Prompt.07Apr19.364255.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.diboson.Prompt.07Apr19.364288.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.diboson.Prompt.07Apr19.364289.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.diboson.Prompt.07Apr19.364290.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.diboson.Prompt.07Apr19.363355.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.diboson.Prompt.07Apr19.363356.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.diboson.Prompt.07Apr19.363357.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.diboson.Prompt.07Apr19.363358.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.diboson.Prompt.07Apr19.363489.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.diboson.Prompt.07Apr19.363494.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.diboson.Prompt.07Apr19.364283.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.diboson.Prompt.07Apr19.364284.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.diboson.Prompt.07Apr19.364285.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.diboson.Prompt.07Apr19.364287.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.diboson.Prompt.07Apr19.345705.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.diboson.Prompt.07Apr19.345706.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.diboson.Prompt.07Apr19.345723.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.singletop.Prompt.25Feb19.410658.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.singletop.Prompt.25Feb19.410659.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.singletop.Prompt.25Feb19.410644.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.singletop.Prompt.25Feb19.410645.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.singletop.Prompt.25Feb19.410646.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.singletop.Prompt.25Feb19.410647.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.ttbar.Prompt.25Feb19.410470.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.dijets.Prompt.07Apr19.364700.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.dijets.Prompt.07Apr19.364701.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.dijets.Prompt.07Apr19.364702.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.dijets.Prompt.07Apr19.364703.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.dijets.Prompt.07Apr19.364704.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.dijets.Prompt.07Apr19.364705.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.dijets.Prompt.07Apr19.364706.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.dijets.Prompt.07Apr19.364707.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.dijets.Prompt.07Apr19.364708.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.dijets.Prompt.07Apr19.364709.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.dijets.Prompt.07Apr19.364710.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.dijets.Prompt.07Apr19.364711.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.dijets.Prompt.07Apr19.364712.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.dijets.Prompt.07Apr19.426001.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.dijets.Prompt.07Apr19.426002.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.dijets.Prompt.07Apr19.426003.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.dijets.Prompt.07Apr19.426004.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.dijets.Prompt.07Apr19.426005.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.dijets.Prompt.07Apr19.426006.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.dijets.Prompt.07Apr19.426007.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.dijets.Prompt.07Apr19.426008.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.dijets.Prompt.07Apr19.426009.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.ttH.Prompt.07Apr19.346343.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.ttH.Prompt.07Apr19.346344.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.ttH.Prompt.07Apr19.346345.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.ttH.Prompt.07Apr19.346343.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.ttH.Prompt.07Apr19.346344.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.ttH.Prompt.07Apr19.346345.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.ttH.Prompt.07Apr19.345940.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.ttH.Prompt.07Apr19.345941.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.ttH.Prompt.07Apr19.345942.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.ttH.Prompt.07Apr19.346346.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.ttH.Prompt.07Apr19.346347.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.ttH.Prompt.07Apr19.346348.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.ttH.Prompt.07Apr19.346222.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.ttH.Prompt.07Apr19.346223.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.ttH.Prompt.07Apr19.346224.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.ttH.Prompt.07Apr19.346225.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.ttH.Prompt.07Apr19.346226.txt',
            '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.ttH.Prompt.07Apr19.346227.txt',
            ]


    plotRates2D(
            selections=config.getSelections(),
            fakeEff=fakeEff,
            realEff=realEff,
            promptEffs=promptEffs,
            lumi=3219.56,
            verbose=True,
            )

    assert isfile('Efficiency_2D/ge2j_pt_eta.root')

    f = ROOT.TFile('Efficiency_2D/ge2j_pt_eta.root')
    hfake = f.Get('FakeEfficiency2D_mu_pt_eta')
    assert hfake.GetEntries != 0
    hreal = f.Get('RealEfficiency2D_mu_pt_eta')
    assert hreal.GetEntries != 0

    fv = ROOT.TFile(join(dirname(__file__), 'ge2j_pt_eta.root'))
    hvfake = fv.Get('FakeEfficiency2D_mu_pt_eta')
    assert hvfake.GetEntries != 0
    hvreal = fv.Get('RealEfficiency2D_mu_pt_eta')
    assert hvreal.GetEntries != 0

    heq(hreal, hvreal)
    heq(hfake, hvfake)

    f.Close()
    fv.Close()
    shutil.rmtree('Efficiency_2D')

    print 'test_verify done.'

if __name__ == '__main__':
    test_verify()

