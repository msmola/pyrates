from PyRates.plotRates import plotRates2D, openTFile, getHisto, getHaddSum, getMCLumi
from PyRates.config import Selection
import ROOT
import os
from os.path import join, dirname
import shutil
import math

def test_basic():

    base = dirname(__file__)

    tf = openTFile(join(base, 'simple.root'))
    assert tf.IsOpen()
    tf = openTFile(join(base, 'eff_basic_1.root'))
    assert tf.IsOpen()

    h = getHisto(
            tf, 
            'Efficiencies_Selection_2',
            'histo2D_Loose_el',
            )
    assert isinstance(h, ROOT.TH2)

    h = h.Clone()
    assert isinstance(h, ROOT.TH2)
    h.SetDirectory(0)
    assert isinstance(h, ROOT.TH2)




    h = getHaddSum(
            (tf,),
            'Efficiencies_Selection_2',
            'histo2D_Loose_el',
            )
    assert isinstance(h, ROOT.TH2)

    tf2 = openTFile(join(base, 'eff_basic_2.root'))
    assert tf.IsOpen()

    h = getHaddSum(
            (tf, tf2),
            'Efficiencies_Selection_2',
            'histo2D_Loose_el',
            )
    assert isinstance(h, ROOT.TH2)

    del tf
    assert isinstance(h, ROOT.TH2)



    assert not os.path.isfile('Efficiency_2D.root'), 'Efficiency_2D.root already exists and would be overwritten by test'

    selections = (
            Selection('Data15_2_el',
                fake='Efficiencies_Data15_2',
                real='Efficiencies_Data15_2',
                prompt='Efficiencies_Data15_2',
                namePass='histo2D_Tight_el',
                nameTot='histo2D_Loose_el',
                lumi=3813,
                param='',
                ),
            Selection('Data15_2_mu',
                fake='Efficiencies_Data15_2',
                real='Efficiencies_Data15_2',
                prompt='Efficiencies_Data15_2',
                namePass='histo2D_Tight_mu',
                nameTot='histo2D_Loose_mu',
                lumi=3813,
                param='',
                ),
            )

    plotRates2D(
            selections=selections,
            fakeEff=join(base, 'data15.Fake.26Jan19.txt'),
            realEff=join(base, 'data15.Real.11Feb19.txt'),
            verbose=True
            )
    assert os.path.isfile('Efficiency_2D/Data15_2_el.root')
    assert os.path.isfile('Efficiency_2D/Data15_2_mu.root')
    os.remove('Efficiency_2D/Data15_2_el.root')
    os.remove('Efficiency_2D/Data15_2_mu.root')
    os.rmdir('Efficiency_2D')

    plotRates2D(
            selections=selections,
            fakeEff='/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.Data15.Fake.26Jan19.txt',
            realEff='/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.Data15.Real.11Feb19.txt',
            promptEffs=(
                '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.WJets.Prompt.28Jan19.364168.txt',
                '/nfs/dust/atlas/user/smolamic/Data/filelists/user.msmola.mc16a.ttbar.Prompt.28Jan19.txt',
                ),
            paint='pdf'
            )

    assert os.path.isfile('Efficiency_2D/Data15_2_el.root')
    assert os.path.isfile('Efficiency_2D/Data15_2_mu.root')
    assert os.path.isfile('Efficiency_2D/FakeEfficiency2D_Data15_2_el.pdf')
    assert os.path.isfile('Efficiency_2D/RealEfficiency2D_Data15_2_el.pdf')
    assert os.path.isfile('Efficiency_2D/FakeEfficiency2D_Data15_2_mu.pdf')
    assert os.path.isfile('Efficiency_2D/RealEfficiency2D_Data15_2_mu.pdf')

    f = ROOT.TFile('Efficiency_2D/Data15_2_el.root')
    assert isinstance(f.Get('FakeEfficiency2D_pt_eta'), ROOT.TH2)
    h_real = f.Get('RealEfficiency2D_pt_eta')
    assert h_real.GetEntries() != 0
    f.Close()

    shutil.rmtree('Efficiency_2D')

    print "test_basic done."

if __name__ == '__main__':
    test_basic()
