SUB .el
namePass histo2D_Tight_el
nameTot histo2D_Loose_el

SUB .mu
namePass histo2D_Tight_mu
nameTot histo2D_Loose_mu

SUB .base
lumi 3813

FOR param IN pt_eta pt_dr

SELECTION el_ge2j_{param}
.base
.el
fake Efficiencies_Fake1L_Fake1e_ge2j_2015
real Efficiencies_Real_Real2LOS_ge2j_2015
prompt Efficiencies_Fake1L_Fake1e_ge2j_2015
param {param}

SELECTION mu_ge2j_{param}
.base
.mu
fake Efficiencies_Fake1L_Fake1mu_ge2j_2015
real Efficiencies_Real_Real2LOS_ge2j_2015
prompt Efficiencies_Fake1L_Fake1mu_ge2j_2015
param {param}

ENDFOR
