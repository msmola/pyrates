from PyRates.plotRates import getScaledPromptHistos, openTFile
from PyRates.config import Config, Selection
from os.path import dirname, join
import ROOT

def test_config():
    base = dirname(__file__)
    config = Config(join(base, 'config.txt'))
    assert isinstance(config, Config)
    assert isinstance(config.getSelections()[0], Selection)

    assert len(config.getSelections()) == 4


    tfiles = [openTFile(f) for f in (
        join(base, 'data15.Fake.25Feb19.root'),
        join(base, 'mc16a.WJets.Prompt.25Feb19.364156.root'),
        )]
    for s in config.getSelections():
        (th_pass, th_tot) = getScaledPromptHistos(tfiles, s)
        assert isinstance(th_pass, ROOT.TH2)
        assert isinstance(th_tot, ROOT.TH2)

    
    print 'test_config done.'

if __name__ == '__main__':
    test_config()

