from test_basic import test_basic
from test_config import test_config
from test_advanced import test_advanced
from test_verify import test_verify
from test_rebin import test_rebin

test_basic()
test_config()
test_advanced()
test_verify()
test_rebin()
