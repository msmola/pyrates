import sys
import os
sys.argv.append('-b')
import ROOT
from progress import ProgressBar, ProgressCounter
from array import array

logVars = ('pt','jetpt','met')

def printHisto(th):
    res = ''
    for xbin in range(th.GetNbinsX()+1):
        res += ' {}'.format(th.GetBinContent(xbin))
    print res

def openTFile(path, mode='READ'):
    tf = ROOT.TFile(path, mode)
    if not tf.IsOpen():
        raise IOError("Failed to open TFile '{}'".format(path))
    return tf

def getHisto(tf, selection, histo, param = None):
    hname = (
            '{selection}/{histo}_{param}'.format(
                selection=selection,
                histo=histo,
                param=param,
                )
            if param else '{selection}/{histo}'.format(
                selection=selection,
                histo=histo
                )
            )

    th = tf.Get(hname)
    if not isinstance(th, ROOT.TH1):
        raise KeyError("Name '{}' in '{}' either not found or not a histogram".format(hname, tf.GetName()))
    return th

def getMCLumi(tf):
    th = tf.Get('MCLumiHist')
    return float(th.GetBinContent(1))

def getMCLumiSum(tfiles):
    return sum([getMCLumi(tf) for tf in tfiles])

def getHaddSum(tfiles, selections, histos, param = None):
    histos = [histo.strip() for histo in histos.split(',')]
    selections = [selection.strip() for selection in selections.split(',')]
    th = getHisto(tfiles[0], selections[0], histos[0], param).Clone()
    th.SetDirectory(0)
    for histo in histos[1:]:
        th.Add(getHisto(tfiles[0], selections[0], histo, param))
    for selection in selections[1:]:
        for histo in histos:
            th.Add(getHisto(tfiles[0], selection, histo, param))
    for tf in tfiles[1:]:
        for selection in selections:
            for histo in histos:
                th.Add(getHisto(tf, selection, histo, param))
    return th

def getScaledPromptHistos(tfiles, selection):
    th_pass = getHaddSum(
            tfiles,
            selection.get('prompt'),
            selection.get('namePass'),
            selection.get('param'),
            )
    th_tot = getHaddSum(
            tfiles,
            selection.get('prompt'),
            selection.get('nameTot'),
            selection.get('param'),
            )
    mc_lumi = getMCLumiSum(tfiles)

    scale = float(selection.get('lumi')) / mc_lumi
    th_pass.Scale(scale)
    th_tot.Scale(scale)

    return (th_pass, th_tot)

def rebin2D(histo, bins):
    xbins = array('d', bins.x)
    ybins = array('d', bins.y)
    re = ROOT.TH2D(
            histo.GetName() + '_rebin',
            '{};{};{};{}'.format(
                histo.GetTitle(),
                histo.GetXaxis().GetTitle(),
                histo.GetYaxis().GetTitle(),
                histo.GetZaxis().GetTitle(),
                ),
            len(xbins) - 1,
            xbins,
            len(ybins) - 1,
            ybins,
            )

    xaxis = histo.GetXaxis()
    yaxis = histo.GetYaxis()
    for xbin in range(1, histo.GetNbinsX() + 1):
        for ybin in range(1, histo.GetNbinsY() + 1):
            re.Fill(
                    xaxis.GetBinLowEdge(xbin),
                    yaxis.GetBinLowEdge(ybin),
                    histo.GetBinContent(xbin, ybin),
                    )
    return re

            

defaultDir = 'Efficiency_2D'

def plotRates2D(
        selections,
        fakeEff,
        realEff,
        promptEffs=(),
        dirName=defaultDir,
        prefix='',
        paint=None,
        lumi=None,
        quiet=False,
        verbose=False,
        binnings={},
        ):

    assert fakeEff != None
    assert realEff != None

    print (
        'plotting Rates\n'
        'fakeEfficiencies from {}\n'
        'realEfficiencies from {}\n'
        '{}\n'
        'for selections\n{}\n'
        'to dir {}.\n'
        ).format(
                fakeEff,
                realEff,
                'promptEfficiencies from\n{}'.format('\n'.join(promptEffs)) if promptEffs else 'without promptEfficiencies',
                '\n'.join([s.name for s in selections]),
                dirName
                )

    steps = len(selections) * (len(promptEffs) + 3)
    progress = ProgressCounter(steps) if quiet else ProgressBar(steps)

    if not os.path.exists(dirName):
        os.makedirs(dirName)

    files = []

    for selection in selections:
        progress.notify('Processing selection {}'.format(selection.name))

        selection.hasKeys('fake', 'real', 'namePass', 'nameTot')
        if not lumi:
            selection.hasKeys('lumi')
        selection.default(
                param=None,
                outfile='{}.root'.format(selection.name),
                lepton=None,
                lumi=lumi,
                binning=None,
                )

        if len(promptEffs):
            progress.notify('Reading prompt-efficiencies from {}'.format(promptEffs[0]))
            with open(promptEffs[0], 'r') as promptList:
                promptFiles = [openTFile(f.strip()) for f in promptList]


            (prompt_pass, prompt_tot) = getScaledPromptHistos(
                    promptFiles,
                    selection,
                    )
            if verbose:
                progress.notify('Entries total: {}, pass: {}'.format(prompt_tot.GetEntries(), prompt_pass.GetEntries()))

            for tf in promptFiles:
                tf.Close()

            progress.advance()
            
            for promptEff in promptEffs[1:]:
                progress.notify('Reading prompt-efficiencies from {}'.format(promptEff))
                with open(promptEff, 'r') as promptList:
                    promptFiles = [openTFile(f.strip()) for f in promptList]

                (th_pass, th_tot) = getScaledPromptHistos(
                        promptFiles,
                        selection,
                        )

                if verbose:
                    progress.notify('Entries total: {}, pass: {}'.format(th_tot.GetEntries(), th_pass.GetEntries()))

                prompt_pass.Add(th_pass)
                prompt_tot.Add(th_tot)

                for tf in promptFiles:
                    tf.Close()

                progress.advance()

        progress.notify('Reading fake-efficiencies from {}'.format(fakeEff))
        with open(fakeEff, 'r') as fakeList:
            fakeFiles = [openTFile(f.strip()) for f in fakeList]


        fake_pass = getHaddSum(
                fakeFiles,
                selection.get('fake'),
                selection.get('namePass'),
                selection.get('param'),
                )

        fake_tot = getHaddSum(
                fakeFiles,
                selection.get('fake'),
                selection.get('nameTot'),
                selection.get('param'),
                )
        if verbose:
            progress.notify('Entries total: {}, pass: {}'.format(fake_tot.GetEntries(), fake_pass.GetEntries()))

        for ff in fakeFiles:
            ff.Close()

        progress.advance()

        progress.notify('Reading real-efficiencies from {}'.format(realEff))
        with open(realEff, 'r') as realList:
            realFiles = [openTFile(f.strip()) for f in realList]

        real_pass = getHaddSum(
                realFiles,
                selection.get('real'),
                selection.get('namePass'),
                selection.get('param'),
                )
        real_tot = getHaddSum(
                realFiles,
                selection.get('real'),
                selection.get('nameTot'),
                selection.get('param'),
                )

        if verbose:
            progress.notify('Entries total: {}, pass: {}'.format(real_tot.GetEntries(), real_pass.GetEntries()))

        for rf in realFiles:
            rf.Close()

        progress.advance()
        progress.notify('Summing histograms')

        if len(promptEffs):
            if verbose:
                progress.notify('Subtracting prompts, total: {}, pass: {}'.format(prompt_tot.GetEntries(), prompt_pass.GetEntries()))
            fake_pass.Add(prompt_pass, -1)
            fake_tot.Add(prompt_tot, -1)


        for xbin in range(0, fake_pass.GetNbinsX() + 1):
            for ybin in range(0, fake_pass.GetNbinsY() + 1):
                v = fake_pass.GetBinContent(xbin, ybin)
                if v < 0:
                    progress.warn('In {}/{}: bin ({}, {}) has a negative value: {}'.format(
                        selection.name,
                        fake_pass.GetName(),
                        xbin,
                        ybin,
                        v
                        ))
        for xbin in range(0, fake_tot.GetNbinsX() + 1):
            for ybin in range(0, fake_tot.GetNbinsY() + 1):
                v = fake_tot.GetBinContent(xbin, ybin)
                if v < 0:
                    progress.warn('In {}/{}: bin ({}, {}) has a negative value: {}'.format(
                        selection.name,
                        fake_tot.GetName(),
                        xbin,
                        ybin,
                        v
                        ))


        param = selection.get('param') or 'pt_eta'
        lparam = '{}_{}'.format(selection.get('lepton'), param) if selection.get('lepton') else param
        x_label, y_label = param.split('_')

        lxparam = '{}_{}'.format(selection.get('lepton'), x_label) if selection.get('lepton') else x_label
        lyparam = '{}_{}'.format(selection.get('lepton'), y_label) if selection.get('lepton') else y_label

        h_fake_name = "FakeEfficiency2D_{}".format(selection.name)
        hx_fake_name = "FakeEfficiency_{}_{}".format(selection.name, x_label)
        hy_fake_name = "FakeEfficiency_{}_{}".format(selection.name, y_label)

        fake_pass_x = fake_pass.ProjectionX()
        fake_pass_x.SetName("FakeEfficiency_{}".format(lxparam))
        fake_pass_x.SetTitle("{};{};#epsilon_f".format(hx_fake_name, x_label))
        fake_pass_y = fake_pass.ProjectionY()
        fake_pass_y.SetName("FakeEfficiency_{}".format(lyparam))
        fake_pass_y.SetTitle("{};{};#epsilon_f".format(hy_fake_name, y_label))
        fake_tot_x = fake_tot.ProjectionX()
        fake_tot_x.SetName("FakeTot_{}".format(lxparam))
        fake_tot_y = fake_tot.ProjectionY()
        fake_tot_y.SetName("FakeTot_{}".format(lyparam))

        h_real_name = "RealEfficiency2D_{}".format(selection.name)
        hx_real_name = "RealEfficiency_{}_{}".format(selection.name, x_label)
        hy_real_name = "RealEfficiency_{}_{}".format(selection.name, y_label)

        real_pass_x = real_pass.ProjectionX()
        real_pass_x.SetName("RealEfficiency_{}".format(lxparam))
        real_pass_x.SetTitle("{};{};#epsilon_r".format(hx_real_name, x_label))
        real_pass_y = real_pass.ProjectionY()
        real_pass_y.SetName("RealEfficiency_{}".format(lyparam))
        real_pass_y.SetTitle("{};{};#epsilon_r".format(hy_real_name, y_label))
        real_tot_x = real_tot.ProjectionX()
        real_tot_x.SetName("RealTot_{}".format(lxparam))
        real_tot_y = real_tot.ProjectionY()
        real_tot_y.SetName("RealTot_{}".format(lyparam))

        fake_pass.SetName('fake_pass_{}'.format(selection.name))
        real_pass.SetName('real_pass_{}'.format(selection.name))
        fake_tot.SetName('fake_tot_{}'.format(selection.name))
        real_tot.SetName('real_tot_{}'.format(selection.name))

        if selection.get('binning'):
            if selection.get('binning') not in binnings:
                raise KeyError(
                        'Trying to use undefined binning {binning} '
                        'in selection {selection}'.format(
                            binning=selection.get('binning'),
                            selection=selection.name,
                            )
                        )
            binning=binnings[selection.get('binning')]
            fake_pass = rebin2D(fake_pass, binning)
            fake_tot = rebin2D(fake_tot, binning)
            real_pass = rebin2D(real_pass, binning)
            real_tot = rebin2D(real_tot, binning)

        fake_pass.SetName("FakeEfficiency2D_{}".format(lparam))
        fake_pass.SetTitle("{};{};{};#epsilon_f".format(h_fake_name, x_label, y_label))
        fake_pass.SetOption('COLZ TEXT')
        fake_pass.SetStats(0)

        real_pass.SetName("RealEfficiency2D_{}".format(lparam))
        real_pass.SetTitle("{};{};{};#epsilon_r".format(h_real_name, x_label, y_label))
        real_pass.SetOption('COLZ TEXT')
        real_pass.SetStats(0)

        
        fake_pass.Divide(fake_tot)
        real_pass.Divide(real_tot)
        fake_pass_x.Divide(fake_tot_x)
        fake_pass_y.Divide(fake_tot_y)
        real_pass_x.Divide(real_tot_x)
        real_pass_y.Divide(real_tot_y)

        if paint:
            c_fake = ROOT.TCanvas(h_fake_name, h_fake_name, 800, 600)
            fake_pass.Draw()
            if x_label in logVars:
                c_fake.SetLogx(1)
            if y_label in logVars:
                c_fake.SetLogy(1)
            c_fake.SaveAs(os.path.join(dirName, '{}.{}'.format(h_fake_name, paint)))

        if paint:
            c_real = ROOT.TCanvas(h_real_name, h_real_name, 800, 600)
            real_pass.Draw()
            if x_label in logVars:
                c_real.SetLogx(1)
            if y_label in logVars:
                c_real.SetLogy(1)
            c_real.SaveAs(os.path.join(dirName, '{}.{}'.format(h_real_name, paint)))

        fname = os.path.join(dirName, '{}{}'.format(prefix, selection.get('outfile')))
        fout = openTFile(fname, 'UPDATE' if (fname in files) else 'RECREATE')
        files.append(fname)
        fout.Append(fake_pass)
        fout.Append(real_pass)
        fout.Append(fake_pass_x)
        fout.Append(real_pass_x)
        fout.Append(fake_pass_y)
        fout.Append(real_pass_y)
        fout.Write()
        fout.Close()

        progress.advance()
    
    progress.finish('done.')
